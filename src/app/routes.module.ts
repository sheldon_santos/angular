import { UserFormComponent } from './packages/components/user-form/user-form.component';
import { LoginComponent } from './packages/components/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const rotas: Routes =[
    {path:'', component: LoginComponent},
    {path:'user', component: UserFormComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(rotas)
    ],
    exports: [RouterModule]
})
export class RoutingModule { 
    
}