import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {
  loading: boolean = false;
  constructor() { }

  ngOnInit() {
    this.loading = false;
  }

}
